//
//  AdController.h
//  iHorn
//
//  Created by Dave Sferra on 11-10-22.
//  Copyright (c) 2011 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <iAd/iAd.h>
#import "MobclixAds.h"
#import "AppDelegate.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface AdController : UIViewController < ADBannerViewDelegate, MobclixAdViewDelegate>{
    BOOL refillCheck;
    MobclixAdView *adUnit;
    ADBannerView *iadView;
    CGRect saveFrame;
}
-(void)callMobClix;
- (id)initWithFrame:(CGRect)aRect;
-(void)calliAd;
-(void)start;
+ (BOOL)isDataSourceAvailable;

@property(nonatomic,retain) IBOutlet MobclixAdView*	adUnit;
@end
