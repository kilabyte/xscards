//
//  main.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-07.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
