//
//  AdController.m
//  iHorn
//
//  Created by Dave Sferra on 11-10-22.
//  Copyright (c) 2011 Blue Hawk Solutions inc. All rights reserved.
//

#import "AdController.h"

@implementation AdController
@synthesize adUnit;
static BOOL _isDataSourceAvailable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithFrame:(CGRect)aRect {
    self = [super init];
    if (self) {
        self.view.frame = aRect;
        saveFrame = aRect;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad{
    [super viewDidLoad];
    [self start];
    self.view.backgroundColor = [UIColor clearColor];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)start{
    if ([AdController isDataSourceAvailable]) {
        if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4.0) {
            int rand = arc4random()%10;
            if (rand <7 && rand >0) {
                    [self calliAd];
            }
            else {
               [self calliAd];// [self callMobClix];
            }
        }
        else {
            [self calliAd];// [self callMobClix];
        }
        
    }
}

-(void)dealloc{
    [self.adUnit cancelAd];
    self.adUnit.delegate = nil;
    self.adUnit = nil;
    [super dealloc];
}


- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
	NSLog(@"iAd Loaded");
	[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
	// assumes the banner view is offset 50 pixels so that it is not visible.
	banner.alpha = 1.0;
	[UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
	NSLog(@"iAd Failed");
	[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
	// assumes the banner view is at the top of the screen.
	banner.alpha = 0.0;
	[UIView commitAnimations];
    [self callMobClix];
	
}
- (void)bannerViewActionDidFinish:(ADBannerView *)banner{
    [iadView removeFromSuperview];
    iadView = nil;
    [self callMobClix];
}

-(void)calliAd{
    self.view.frame = saveFrame;
    if (iadView) {
        [iadView removeFromSuperview];
        iadView = nil;
    }
    iadView = [[ADBannerView alloc] initWithFrame:saveFrame];
    iadView.delegate = self;
    [self.view addSubview: iadView];
    [iadView release];
}


-(void)callMobClix{
    self.view.frame = saveFrame;
    self.adUnit = [[MobclixAdViewiPhone_320x50 alloc] initWithFrame:saveFrame];
    self.adUnit.delegate = self;
	[self.view addSubview:self.adUnit];
    [self.adUnit resumeAdAutoRefresh];
}

#pragma mark -
#pragma mark MobclixAdViewDelegate Methods

- (void)adViewDidFinishLoad:(MobclixAdView*)adView {
	NSLog(@"Ad Loaded: %@.", NSStringFromCGSize(adView.frame.size));
}

- (void)adView:(MobclixAdView*)adView didFailLoadWithError:(NSError*)error {
	NSLog(@"Ad Failed: %@.", NSStringFromCGSize(adView.frame.size));
    [self.adUnit pauseAdAutoRefresh];
    self.adUnit = nil;
    [self calliAd];
}

- (void)adViewWillTouchThrough:(MobclixAdView*)adView {
	NSLog(@"Ad Will Touch Through: %@.", NSStringFromCGRect(adView.frame));
}

- (void)adViewDidFinishTouchThrough:(MobclixAdView*)adView {
	NSLog(@"Ad Did Finish Touch Through: %@.", NSStringFromCGRect(adView.frame));
    [self.adUnit pauseAdAutoRefresh];
    [self.adUnit removeFromSuperview];
    self.adUnit = nil;
    [self calliAd];
}


+ (BOOL)isDataSourceAvailable
{
    static BOOL checkNetwork = YES;
    if (checkNetwork) { // Since checking the reachability of a host can be expensive, cache the result and perform the reachability check once.
        checkNetwork = NO;
        
        BOOL success;    
		
		const char *host_name = "bluehawksolutions.com";
		
        SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host_name);
        SCNetworkReachabilityFlags flags;
        success = SCNetworkReachabilityGetFlags(reachability, &flags);
        _isDataSourceAvailable = success && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
        CFRelease(reachability);
    }
    return _isDataSourceAvailable;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
return NO;
}

@end
