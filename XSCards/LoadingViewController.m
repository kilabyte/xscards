//
//  LoadingViewController.m
//  CoastLinePilots
//
//  Created by Dave Sferra on 11-07-17.
//  Copyright 2011 Blue Hawk Solutions inc. All rights reserved.
//

#import "LoadingViewController.h"

@implementation LoadingViewController

@synthesize useFixedTime, fixedTime;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        fixedTime = 3;
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2){
        //iPhone 4
        [_mainImage setImage:[UIImage imageNamed:@"Default@2x.png"]];
    }
    else
    {
        [_mainImage setImage:[UIImage imageNamed:@"Default.png"]];
    }
    // Do any additional setup after loading the view from its nib.
    //call timer here to fade out
    if (useFixedTime) {
        [self performSelector:@selector(finished) withObject:nil afterDelay:fixedTime];
    }
}

-(void)finished{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDidStopSelector:@selector(cleanOut)];
    self.view.alpha = 0.0;
    [UIView commitAnimations];
    
    
}


-(void)cleanOut{
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
return NO;
}

@end
