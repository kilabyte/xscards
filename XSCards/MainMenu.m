//
//  MainMenu.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-07.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "MainMenu.h"
#import "BHTableViewCell.h"
#import "BHTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

#define BUTTON_LEFT_MARGIN 85 //10
#define BUTTON_SPACING 75 //25

@interface MainMenu (PrivateStuff)
-(void) setupSideSwipeView;
-(UIImage*) imageFilledWith:(UIColor*)color using:(UIImage*)startImage;
@end

@implementation MainMenu

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    buttonData = [[NSArray arrayWithObjects:
                   //[NSDictionary dictionaryWithObjectsAndKeys:@"Share", @"title", @"action.png", @"image", nil],
                   [NSDictionary dictionaryWithObjectsAndKeys:@"Delete", @"title", @"UIButtonBarTrash@2x.png", @"image", nil],
                   [NSDictionary dictionaryWithObjectsAndKeys:@"Edit", @"title", @"retweet-outline-button-item@2x.png", @"image", nil],
                   nil] retain];
    buttons = [[NSMutableArray alloc] initWithCapacity:buttonData.count];
    
    self.sideSwipeView = [[[UIView alloc] initWithFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, tableView.rowHeight)] autorelease];
    [self setupSideSwipeView];
    
    
    UIImageView *title = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    [title setFrame:CGRectMake(0, 0, 200, 37)];
    self.navigationItem.titleView = title;
    [title release];
    
    iadView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-53, 320, 50)];
    iadView.delegate = self;
    [self.view addSubview: iadView];
    
    
    
    [addButton setFrame:CGRectMake(32, iadView.frame.origin.y-120, 259, 45)];
    [editButton setFrame:CGRectMake(200, iadView.frame.origin.y-97, 91, 45)];
    [helpButton setFrame:CGRectMake(32, iadView.frame.origin.y-52, 91, 45)];
    [infoButton setFrame:CGRectMake(151, iadView.frame.origin.y-26, 18, 19)];
    [settingsButton setFrame:CGRectMake(200, iadView.frame.origin.y-52, 91, 45)];
    [swipeImage setFrame:CGRectMake(0, iadView.frame.origin.y-160, 320, 190)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLock) name:@"LockOn" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable:) name:@"ReloadCards" object:nil];
    [self showLock];
    //setup the mainmenu table
    tableView.backgroundColor = [UIColor clearColor];
    tableView.allowsSelectionDuringEditing = YES;
    tableView.separatorColor = [UIColor colorWithRed:0.49411765 green:0.68627451 blue:0.22352941 alpha:1.0];
    _savedCards = [[NSMutableArray alloc] init];
    a = appDelegate;
    a.adRect = iadView.frame;
    [_savedCards removeAllObjects];
    [_savedCards addObjectsFromArray:a._storedCards];
    UISwipeGestureRecognizer *oneFingerSwipeUp = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openDrawer)] autorelease];
    [oneFingerSwipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:oneFingerSwipeUp];
    
    UISwipeGestureRecognizer *oneFingerSwipeDown = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeDrawer)] autorelease];
    [oneFingerSwipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [[self view] addGestureRecognizer:oneFingerSwipeDown];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
	NSLog(@"iAd Loaded");
	[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
	banner.alpha = 1.0;
	[UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
	NSLog(@"iAd Failed");
	[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
    banner.alpha = 0.0;
    [UIView commitAnimations];
}


-(void)showLock{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL check = [defaults boolForKey:@"PasswordState"];
    if (check) {//a.shouldShowLock BOOL
        PassScreenViewController *p = [[PassScreenViewController alloc] initWithNibName:@"PassScreenViewController" bundle:nil];
        p.shouldSet = NO;
        [self presentModalViewController:p animated:YES];
        [p release];
    }
    a.shouldShowLock = check;
}

-(void)dealloc{
    [super dealloc];
    [ads release];
}

-(IBAction)reloadTable:(id)sender{
    [_savedCards removeAllObjects];
    [_savedCards addObjectsFromArray:a._storedCards];
    NSSortDescriptor *sorter;
    sorter = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];
    [_savedCards sortUsingDescriptors:sortDescriptors];
    [sorter release];
    [tableView reloadData];
    a.sendNewCard = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (tableView.isEditing) {
        tableView.editing = NO;
        tableView.backgroundColor = [UIColor clearColor];
    }
    if (isSending == NO && a.sendNewCard == NO) {
        [_savedCards removeAllObjects];
        [_savedCards addObjectsFromArray:a._storedCards];
        NSSortDescriptor *sorter;
        sorter = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];
        [_savedCards sortUsingDescriptors:sortDescriptors];
        [sorter release];
        [tableView reloadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return NO;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    if ([_savedCards count] < 1) {
        [editButton setEnabled:NO];
        [tableView setHidden:YES];
        [placeholderImage setHidden:NO];
        return 0;
    }
    else {
        [tableView setHidden:NO];
        [placeholderImage setHidden:YES];
        return [_savedCards count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    BHTableViewCell *cell = (BHTableViewCell*)[theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[[BHTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    
    // Configure the cell...
        cell.textLabel.textColor = [UIColor whiteColor];
        Card *c = [[Card alloc] init];
        c = [_savedCards objectAtIndex:indexPath.row];
    if (c != nil) {
        cell.textLabel.text = c.name;
        cell.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"background.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];  
        if (c.image) {
            cell.imageView.image = [c.image imageScaledToSize:CGSizeMake(65, 60)];
        }
        else {
            cell.imageView.image = nil;
        }
        cell.imageView.layer.masksToBounds = YES;
        cell.imageView.layer.cornerRadius = 5.0;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [editButton setEnabled:YES];
        cell.supressDeleteButton = ![self gestureRecognizersSupported];
    }

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}
- (void) setupSideSwipeView{
    // Add the background pattern
    self.sideSwipeView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"dotted-pattern.png"]];
    
    // Overlay a shadow image that adds a subtle darker drop shadow around the edges
    UIImage* shadow = [[UIImage imageNamed:@"inner-shadow.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    UIImageView* shadowImageView = [[[UIImageView alloc] initWithFrame:sideSwipeView.frame] autorelease];
    shadowImageView.alpha = 0.6;
    shadowImageView.image = shadow;
    shadowImageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.sideSwipeView addSubview:shadowImageView];
    
    // Iterate through the button data and create a button for each entry
    CGFloat leftEdge = BUTTON_LEFT_MARGIN;
    for (NSDictionary* buttonInfo in buttonData)
    {
        // Create the button
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        // Make sure the button ends up in the right place when the cell is resized
        button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        
        // Get the button image
        UIImage* buttonImage = [UIImage imageNamed:[buttonInfo objectForKey:@"image"]];
        
        // Set the button's frame
        button.frame = CGRectMake(leftEdge, sideSwipeView.center.y - buttonImage.size.height/2.0, buttonImage.size.width, buttonImage.size.height);
        
        // Add the image as the button's background image
        // [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
        UIImage* grayImage = [self imageFilledWith:[UIColor colorWithWhite:0.9 alpha:1.0] using:buttonImage];
        [button setImage:grayImage forState:UIControlStateNormal];
        
        // Add a touch up inside action
        [button addTarget:self action:@selector(touchUpInsideAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // Keep track of the buttons so we know the proper text to display in the touch up inside action
        [buttons addObject:button];
        
        // Add the button to the side swipe view
        [self.sideSwipeView addSubview:button];
        
        // Move the left edge in prepartion for the next button
        leftEdge = leftEdge + buttonImage.size.width + BUTTON_SPACING;
    }
}

#pragma mark Button touch up inside action
- (IBAction) touchUpInsideAction:(UIButton*)button{
    NSIndexPath* indexPath = [tableView indexPathForCell:sideSwipeCell];
    
    NSUInteger index = [buttons indexOfObject:button];
    NSDictionary* buttonInfo = [buttonData objectAtIndex:index];
    
    if ([[buttonInfo objectForKey:@"title"] isEqualToString:@"Share"]) {
        [self showPicker:indexPath];
    }
    
    if ([[buttonInfo objectForKey:@"title"] isEqualToString:@"Delete"]) {
        [self deleteCardAtIndex:indexPath];
    }
    
    if ([[buttonInfo objectForKey:@"title"] isEqualToString:@"Edit"]) {
        [self editCardAtIndexPath:indexPath];
    }
    [self removeSideSwipeView:YES];
}

#pragma mark Generate images with given fill color
// Convert the image's fill color to the passed in color
-(UIImage*) imageFilledWith:(UIColor*)color using:(UIImage*)startImage
{
    // Create the proper sized rect
    CGRect imageRect = CGRectMake(0, 0, CGImageGetWidth(startImage.CGImage), CGImageGetHeight(startImage.CGImage));
    
    // Create a new bitmap context
    CGContextRef context = CGBitmapContextCreate(NULL, imageRect.size.width, imageRect.size.height, 8, 0, CGImageGetColorSpace(startImage.CGImage), kCGImageAlphaPremultipliedLast);
    
    // Use the passed in image as a clipping mask
    CGContextClipToMask(context, imageRect, startImage.CGImage);
    // Set the fill color
    CGContextSetFillColorWithColor(context, color.CGColor);
    // Fill with color
    CGContextFillRect(context, imageRect);
    
    // Generate a new image
    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    UIImage* newImage = [UIImage imageWithCGImage:newCGImage scale:startImage.scale orientation:startImage.imageOrientation];
    
    // Cleanup
    CGContextRelease(context);
    CGImageRelease(newCGImage);
    
    return newImage;
}


-(UIImage*)resizedImage1:(UIImage*)inImage  inRect:(CGRect)thumbRect {
	UIGraphicsBeginImageContext(thumbRect.size);
	[inImage drawInRect:thumbRect];
	return UIGraphicsGetImageFromCurrentImageContext();
}

-(void)deleteCardAtIndex:(NSIndexPath*)indexPath{
    [a._storedCards removeObjectAtIndex:indexPath.row];
    [_savedCards removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)theTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [a._storedCards removeObjectAtIndex:indexPath.row];
        [_savedCards removeObjectAtIndex:indexPath.row];
        [theTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [theTableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([_savedCards count]>0 && tableView.isEditing == NO) {
        Card *g = [[Card alloc] init];
        g = [_savedCards objectAtIndex:indexPath.row];
        a._mode = g.mode;
        a._cardInputed = g.number;
        a._image = g.image;
        BarcodeViewController *m = [[BarcodeViewController alloc] initWithNibName:@"BarcodeViewController" bundle:nil];
        m._caption.text = g.number;
        m.nameTitle = g.name;
        [self presentModalViewController:m animated:YES];
        [m release];
    }
    
    if ([_savedCards count]>0 && tableView.isEditing == YES) {
        Card *g = [[Card alloc] init];
        g = [_savedCards objectAtIndex:indexPath.row];
        AddCard *ac = [[AddCard alloc] initWithNibName:@"AddCard" bundle:nil];
        ac.editCard = g;
        ac.editIndex = indexPath.row;
        ac.isEditing = YES;
        [self presentModalViewController:ac animated:YES];
        [ac release];
    }


}

-(void)editCardAtIndexPath:(NSIndexPath*)indexPath{
    if ([_savedCards count]>0) {
        Card *g = [[Card alloc] init];
        g = [_savedCards objectAtIndex:indexPath.row];
        AddCard *ac = [[AddCard alloc] initWithNibName:@"AddCard" bundle:nil];
        ac.editCard = g;
        ac.editIndex = indexPath.row;
        ac.isEditing = YES;
        [self presentModalViewController:ac animated:YES];
        [ac release];
    }
}

-(void)openCloseDrawer{
    static BOOL isOpen = YES; //drawer is open by default
    if (!isOpen) {
        isOpen = YES;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.75];
        [addButton setAlpha:1.0];
        [editButton setAlpha:1.0];
        [helpButton setAlpha:1.0];
        [infoButton setAlpha:1.0];
        [settingsButton setAlpha:1.0];
        [tableView setBounds:CGRectMake(0, 0, 320, 229)];
        [tableView setFrame:CGRectMake(0, 0, 320, 229)];
        [UIView commitAnimations];
    }
    else {
        isOpen = NO;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.75];
        [addButton setAlpha:0.0];
        [editButton setAlpha:0.0];
        [helpButton setAlpha:0.0];
        [infoButton setAlpha:0.0];
        [settingsButton setAlpha:0.0];
        [tableView setBounds:CGRectMake(0, 0, 320, 385)];
        [tableView setFrame:CGRectMake(0, 0, 320, 385)];
        [UIView commitAnimations];
    }
}

-(void)openDrawer{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.5];
    
    [addButton setFrame:CGRectMake(32, placeHolder.frame.origin.y-120, 259, 45)];
    [editButton setFrame:CGRectMake(200, placeHolder.frame.origin.y-97, 91, 45)];
    [helpButton setFrame:CGRectMake(32, placeHolder.frame.origin.y-52, 91, 45)];
    [infoButton setFrame:CGRectMake(151, placeHolder.frame.origin.y-26, 18, 19)];
    [settingsButton setFrame:CGRectMake(200, placeHolder.frame.origin.y-52, 91, 45)];
    [swipeImage setFrame:CGRectMake(0, placeHolder.frame.origin.y-160, 320, 190)];
    
    
    [tableView setBounds:CGRectMake(0, 0, 320, placeHolder.frame.origin.y-160)];
    [tableView setFrame:CGRectMake(0, 0, 320, placeHolder.frame.origin.y-160)];

    [UIView commitAnimations];
}

-(void)closeDrawer{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.5];

    [addButton setFrame:CGRectMake(32, 267+205, 259, 45)];
    [editButton setFrame:CGRectMake(200, 267+160, 91, 45)];
    [helpButton setFrame:CGRectMake(32, 332+160, 91, 45)];
    [infoButton setFrame:CGRectMake(151, 316+160, 18, 19)];
    [settingsButton setFrame:CGRectMake(200, 332+160, 91, 45)];
    
    [tableView setBounds:CGRectMake(0, 0, 320, placeHolder.frame.origin.y-40)];
    [tableView setFrame:CGRectMake(0, 0, 320, placeHolder.frame.origin.y-40)];
    [swipeImage setFrame:CGRectMake(0, placeHolder.frame.origin.y-30, 320, 190)];
    [UIView commitAnimations];
}

-(IBAction)addCard:(id)sender{
    AddCard *ac = [[AddCard alloc] initWithNibName:@"AddCard" bundle:nil];
    ac.isEditing = NO;
    [self presentModalViewController:ac animated:YES];
    [ac release];
}
-(IBAction)editCard:(id)sender{
    if (tableView.isEditing) {
        tableView.editing = NO;
        tableView.backgroundColor = [UIColor clearColor];
    }
    else {
        tableView.editing = YES;
        tableView.backgroundColor = [UIColor redColor];
    }
}
-(IBAction)helpCard:(id)sender{
    HelpController *h = [[HelpController alloc] initWithNibName:@"HelpController" bundle:nil];
    [self.navigationController pushViewController:h animated:YES];
    [h release];
    
}
-(IBAction)infoCard:(id)sender{
    InfoController *i = [[InfoController alloc] initWithNibName:@"InfoController" bundle:nil];
    [self.navigationController pushViewController:i animated:YES];
    [i release];
    
}
-(IBAction)settingsCard:(id)sender{
    SettingsController *s = [[SettingsController alloc] initWithNibName:@"SettingsController" bundle:nil];
    [self.navigationController pushViewController:s animated:YES];
    [s release];
}

-(IBAction)showPicker:(NSIndexPath*)indexPath{
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheetWithIndex:indexPath.row];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self displayComposerSheetWithIndex:indexPath.row];
	}
}	

#pragma mark -
#pragma mark Compose Mail
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError withIndex:(int)index{
    if ([typeName isEqualToString:@"Share"]) {
        Card *item = [[Card alloc] init];
        item = [_savedCards objectAtIndex:index];
        //Create a Dictionary
        NSMutableDictionary *dictToSave = [NSMutableDictionary dictionary];
        //Add the first image to the dictionary
        [dictToSave setObject:item forKey:@"SharedCard"];
        //Return the archived data
        return [NSKeyedArchiver archivedDataWithRootObject:item];
    }
    //Don't generate an error
    outError = NULL;
    return nil;
}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(IBAction)displayComposerSheetWithIndex:(int)index {
    isSending = YES;
    Card *item = [[Card alloc] init];
    item = [_savedCards objectAtIndex:index];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.xsc",item.name]];
    [[self dataOfType:@"Share" error:nil withIndex:index] writeToFile:savePath atomically:YES];

	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setSubject:[NSString stringWithFormat:@"Here is a XSCard for you! - %@",item.name]];
    
    // Set up recipients
    //NSArray *toRecipients = [NSArray arrayWithObject:@""]; 
    
    //[picker setToRecipients:toRecipients];
    
    // Attach a doc to the email
    NSData *myData = [NSData dataWithContentsOfFile:savePath];
    [picker addAttachmentData:myData mimeType:@"card/x-xsc" fileName:[NSString stringWithFormat:@"%@.xsc",item.name]];
    
    // Fill out the email body text
    NSString *emailBody = @"Please see the attached document.";
    [picker setMessageBody:emailBody isHTML:NO];
	
	[self presentModalViewController:picker animated:YES];
	[picker release];
    [item release];
}

-(void)changeIsSending{
    isSending = NO;
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Result: canceled");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Result: saved");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Result: sent");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Result: failed");
			break;
		default:
			NSLog(@"Result: not sent");
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
    [self performSelector:@selector(changeIsSending) withObject:nil afterDelay:1.5];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Setup" message:@"You device is currently not setup to send emails. To setup an email account on your device go to : Settings -> Mail -> Add Account and continue with the directions."
												   delegate:self 
										  cancelButtonTitle:@"OK" 
										  otherButtonTitles:nil];
	[alert show];	
	[alert release];
}



@end
