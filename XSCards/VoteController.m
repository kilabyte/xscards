//
//  VoteController.m
//  XSCards
//
//  Created by Dave Sferra on 2012-03-14.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "VoteController.h"
#import "AppDelegate.h"

@interface VoteController ()

@end

@implementation VoteController
@synthesize _cardName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setFrame:CGRectMake(0, 430, 320, 50)];
    }
    return self;
}


-(IBAction)yes:(id)sender{
    [self sendToServerWithResponse:@"YES" andCardName:self._cardName];    
}

-(IBAction)no:(id)sender{
    [self sendToServerWithResponse:@"NO" andCardName:self._cardName];
}


-(void)show{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.75];
    [self.view setFrame:CGRectMake(0, 430, 320, 50)];
    [UIView commitAnimations];
}

-(void)dismiss{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.75];
    [self.view setFrame:CGRectMake(0, 480, 320, 50)];
    [UIView commitAnimations];
}

-(void)sendToServerWithResponse:(NSString*)answer andCardName:(NSString*)cardName{
    if([AppDelegate isDataSourceAvailable]){
        NSString *vendorName = self._cardName;
        NSString *hostString = @"http://interwerx.ca/iOS/reportToCards.php";
        vendorName = [vendorName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *argsString = @"%@?card=%@&answer=%@";
        NSString *getURLString = [NSString stringWithFormat:argsString, hostString, vendorName, answer];
        NSString *registerResult = [NSString stringWithContentsOfURL:[NSURL URLWithString:getURLString] encoding:NSStringEncodingConversionAllowLossy error:nil];
        
#ifdef DEBUG
        NSLog( @"registerResult:%@", registerResult ); 
#endif
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:hostString]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[[[NSData alloc] initWithBytes:[registerResult UTF8String] length:[registerResult length]] autorelease]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [[NSURLConnection connectionWithRequest:request delegate:self] start];
    }

    
    [self dismiss];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
return NO;
}

@end
