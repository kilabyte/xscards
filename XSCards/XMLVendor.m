
#import "XMLVendor.h"

static NSUInteger parsedRSSItemsCounter;

@implementation XMLVendor

@synthesize currentRSSItem = _currentRSSItem;
@synthesize contentOfCurrentRSSItemProperty = _contentOfCurrentRSSItemProperty;
@synthesize results = _results;


// Limit the number of parsed RSSItems to 25 Otherwise the application runs very slowly on the device.
#define MAX_RSSITEMS 25

- (id)init {
	if (self = [super init]) {
		_results = [ [NSMutableArray alloc] init];
	}
	return self;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
}

-(void)parseXMLFileAtURL:(NSURL *)URL{
//	_results = [ [NSMutableArray alloc] init];
	[_results removeAllObjects];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:URL];
	parsedRSSItemsCounter = 0;
	

    // Set self as the delegate of the parser so that it will receive the parser delegate methods callbacks.
    [parser setDelegate:self];
	
    // Depending on the XML document you're parsing, you may want to enable these features of NSXMLParser.
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    
    [parser parse];
	[parser release];
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if (qName) {
        elementName = qName;
    }
   // NSLog(@"%@",elementName);
	
    if ([elementName isEqualToString:@"vendor"]) {
        
        self.contentOfCurrentRSSItemProperty = nil;
        parsedRSSItemsCounter++;
        
		// An entry in the RSS feed represents an earthquake, so create an instance of it.
		VendorItem *p = [[VendorItem alloc] init];
		self.currentRSSItem = p;
		[self addToRSSItemsList:p];

        
	} else if ([elementName isEqualToString:@"name"]
               || [elementName isEqualToString:@"image"]
               || [elementName isEqualToString:@"length"]
               || [elementName isEqualToString:@"anotherLength"]
               || [elementName isEqualToString:@"startsWith"]
               || [elementName isEqualToString:@"barcodeType"]) {
		
        // Create a mutable string to hold the contents of the 'georss:point' element.
        // The contents are collected in parser:foundCharacters:.
        self.contentOfCurrentRSSItemProperty = [NSMutableString string];
		
    } else {
        // The element isn't one that we care about, so set the property that holds the 
        // character content of the current element to nil. That way, in the parser:foundCharacters:
        // callback, the string that the parser reports will be ignored.
        self.contentOfCurrentRSSItemProperty = nil;
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{     
	if (qName) {
		elementName = qName;
	}
	
	NSString *s = [self.contentOfCurrentRSSItemProperty stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	[self.contentOfCurrentRSSItemProperty setString:s];
	
	
	if ([elementName isEqualToString:@"name"]) {
		self.currentRSSItem.name = [NSString stringWithString:self.contentOfCurrentRSSItemProperty];
        
	} else if ([elementName isEqualToString:@"image"]) {
		self.currentRSSItem.image = [NSString stringWithString:self.contentOfCurrentRSSItemProperty];
        
	} else if ([elementName isEqualToString:@"length"]) {
		self.currentRSSItem.length = [NSString stringWithString:self.contentOfCurrentRSSItemProperty];
		
	} else if ([elementName isEqualToString:@"startsWith"]) {
		self.currentRSSItem.startsWith = [NSString stringWithString:self.contentOfCurrentRSSItemProperty];
		
	} else if ([elementName isEqualToString:@"barcodeType"]) {
		self.currentRSSItem.barcodeType = [NSString stringWithString:self.contentOfCurrentRSSItemProperty];
        
	} else if ([elementName isEqualToString:@"anotherLength"]) {
		self.currentRSSItem.anotherLength = [NSString stringWithString:self.contentOfCurrentRSSItemProperty];
	}
	
	
}


- (void)addToRSSItemsList:(VendorItem *)newRSSItem
{
	[_results addObject:newRSSItem];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if (parsedRSSItemsCounter == MAX_RSSITEMS) {
		
		[parser abortParsing];
		return;
    }
	

    if (self.contentOfCurrentRSSItemProperty) {
        // If the current element is one whose content we care about, append 'string'
        // to the property that holds the content of the current element.
		[self.contentOfCurrentRSSItemProperty appendString:string];
    }
}

- (void)dealloc {
	[_currentRSSItem release];
	[_contentOfCurrentRSSItemProperty release];
	[_results release];
	[super dealloc];
}


@end
