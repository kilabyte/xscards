//
//  MKStoreObserver.m
//  MKStoreKit (Version 4.0)
//

#import "MKStoreObserver.h"
#import "MKStoreManager.h"

@interface MKStoreManager (InternalMethods)

// these three functions are called from MKStoreObserver
- (void) transactionCanceled: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;

- (void) provideContent: (NSString*) productIdentifier 
			forReceipt: (NSData*) recieptData;
@end

@implementation MKStoreObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
	for (SKPaymentTransaction *transaction in transactions)
	{
		switch (transaction.transactionState)
		{
			case SKPaymentTransactionStatePurchased:
				
                [self completeTransaction:transaction];
				
                break;
				
            case SKPaymentTransactionStateFailed:
				
                [self failedTransaction:transaction];
				
                break;
				
            case SKPaymentTransactionStateRestored:
				
                [self restoreTransaction:transaction];
				
            default:
				
                break;
		}			
	}
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [[MKStoreManager sharedManager] restoreFailedWithError:error];    
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue 
{
    [[MKStoreManager sharedManager] restoreCompleted];
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction
{	
	[[MKStoreManager sharedManager] transactionCanceled:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction
{			
#if TARGET_OS_IPHONE
    [[MKStoreManager sharedManager] provideContent:transaction.payment.productIdentifier 
									   forReceipt:transaction.transactionReceipt];	
#elif TARGET_OS_MAC
    [[MKStoreManager sharedManager] provideContent:transaction.payment.productIdentifier 
                                        forReceipt:nil];	
#endif

    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{	
#if TARGET_OS_IPHONE
    [[MKStoreManager sharedManager] provideContent: transaction.originalTransaction.payment.productIdentifier
                                        forReceipt:transaction.transactionReceipt];
#elif TARGET_OS_MAC
    [[MKStoreManager sharedManager] provideContent: transaction.originalTransaction.payment.productIdentifier
                                        forReceipt:nil];
#endif
	
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
}

@end
