//
//  BarcodeViewController.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "BarcodeViewController.h"
#import "VoteController.h"

@implementation BarcodeViewController

@synthesize _caption, nameTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)generateBarcodeWithMode:(int)mode{
    
    if (mode == 0) {
        NKDEAN8Barcode *barcode = [[[NKDEAN8Barcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 1) {
        NKDEAN13Barcode *barcode = [[[NKDEAN13Barcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 2) {
        NKDCodabarBarcode *barcode = [[[NKDCodabarBarcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 3) {
        NKDCode39Barcode *barcode = [[[NKDCode39Barcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 4) {
        NKDCode128Barcode *barcode = [[[NKDCode128Barcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 5) {
        NKDUPCABarcode *barcode = [[[NKDUPCABarcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 6) {
        NKDUPCEBarcode *barcode = [[[NKDUPCEBarcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 7) {
        NKDAbstractUPCEANBarcode *barcode = [[[NKDAbstractUPCEANBarcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    else if (mode == 8) {
        NKDInterleavedTwoOfFiveBarcode *barcode = [[[NKDInterleavedTwoOfFiveBarcode alloc] initWithContent:a._cardInputed printsCaption:NO] autorelease];
        [_barCodeView setImage:[UIImage imageFromBarcode:barcode]];
    }
    
    [_caption setText:a._cardInputed];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)dealloc{
    [super dealloc];
    [rView release];
    rView = nil;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    a = appDelegate;
    navBar.topItem.title = nameTitle;
    if (a._cardInputed.length != 0) {
        [self generateBarcodeWithMode:a._mode];
        _vendorLogo.image = a._image;
        modes = a._mode;
        NSLog(@"Card Number %@",a._cardInputed);
    }
    else{
        _caption.text = @"Error";
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL check = [defaults boolForKey:[NSString stringWithFormat:@"Check_%@",nameTitle]];
    if (!check) {
        VoteController *v = [[[VoteController alloc] initWithNibName:@"VoteController" bundle:nil] autorelease];
        v._cardName = nameTitle;
        [self.view addSubview:v.view];
        [v show];
        [defaults setBool:YES forKey:[NSString stringWithFormat:@"Check_%@",nameTitle]];
        [defaults setObject:[NSString stringWithFormat:@"%@",a._cardInputed] forKey:[NSString stringWithFormat:@"CardNo_%@",nameTitle]];
        NSTimer *t;
        t = [NSTimer scheduledTimerWithTimeInterval:7 target:v selector:@selector(dismiss) userInfo:nil repeats:NO];
    }    
    
    [self checkForRotate];
}
#define DegreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )
-(void)checkForRotate{
    [rView setHidden:YES];
    if (a._mode == 3 || a._mode == 4 || a._mode == 8) {
        //[rView setHidden:NO]; 
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationLandscapeRight;
}

-(IBAction)done:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if (a._mode == 3 || a._mode == 4 || a._mode == 8) {
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft | interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    }
    else {
    return NO;
    }
}

@end
