
#import <Foundation/Foundation.h>

#import "VendorItem.h"

@interface XMLVendor : NSObject <NSXMLParserDelegate>{

@private        
    VendorItem*			_currentRSSItem;
    NSMutableString*	_contentOfCurrentRSSItemProperty;
	NSMutableArray*		_results;
}

@property (nonatomic, retain)  VendorItem* currentRSSItem;
@property (nonatomic, retain) NSMutableString* contentOfCurrentRSSItemProperty;
@property (nonatomic, retain) NSMutableArray* results;

- (void)parseXMLFileAtURL:(NSURL *)URL;
- (void)addToRSSItemsList:(VendorItem *)newRSSItem;

@end
