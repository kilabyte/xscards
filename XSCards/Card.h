//
//  Card.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BHImage.h"

@interface Card : NSObject {
    BHImage *image;
    NSString *name;
    NSString *number;
    int mode;

}

@property(nonatomic, retain) BHImage *image;
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSString *number;
@property(assign) int mode;
@end
