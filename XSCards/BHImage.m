//
//  BHImage.m
//  XSCards
//
//  Created by Dave Sferra on 2012-02-19.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "BHImage.h"

@implementation BHImage

- (UIImage*)imageScaledToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
