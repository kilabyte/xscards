//
//  ScannerController.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "AppDelegate.h"

@interface ScannerController : UIViewController<ZBarReaderViewDelegate>{
    ZBarReaderView *readerView;
    NSString *data;
    ZBarCameraSimulator *cameraSim;
    AppDelegate *a;
}

@property (nonatomic, retain) IBOutlet ZBarReaderView *readerView;
@property (nonatomic, retain) IBOutlet NSString *data;

@end
