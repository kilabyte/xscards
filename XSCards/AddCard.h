//
//  AddCard.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"
#import "AppDelegate.h"
#import "Validator.h"
#import "XMLVendor.h"
#import "ScannerController.h"
#import "VendorItem.h"
#import "BHImage.h"

@interface AddCard : UIViewController <UIAlertViewDelegate>{
    IBOutlet UITextField *name;
    IBOutlet UITextField *number;
    IBOutlet UIButton *scanButton;
    IBOutlet UIImageView *startHere;
    IBOutlet UINavigationBar *navBar;
    AppDelegate *a;
    NSMutableArray *barcodeTypes;
    int selectedMode;
    NSString *barcodeType;
    IBOutlet UIBarButtonItem *doneButton;
    BOOL isChecking;
    Validator *v;
    BOOL isScanning;
    
    //Editor
    BOOL isEditing;
    int editIndex;
    Card *editCard;
    BHImage *image;
    BOOL showScanner;
    XMLVendor *xmlVendor;
    NSMutableArray *vendorList;
    int foundIndex;
    BOOL didUseOnline;
    BOOL wait;
}

@property(assign) BOOL isEditing;
@property(assign) int editIndex;
@property(nonatomic, retain) Card *editCard;
-(IBAction)openScanner:(id)sender;
-(void)findBarcodeType;
-(void)validatorWithNoCode:(BOOL)useCode;
-(IBAction)save:(id)sender;
@end
