#import "VendorItem.h"


@implementation VendorItem

@synthesize image = _image;
@synthesize length = _length;
@synthesize name = _name;
@synthesize startsWith = _startsWith;
@synthesize barcodeType = _barcodeType;
@synthesize anotherLength = _anotherLength;

-(id) init{
	if(self = [super init]){
		_name = @"";
		_image = @"";
		_length = @"";
        _startsWith = @"";
        _barcodeType = @"";
        _anotherLength = @"";
	}
	return self;
}

-(void)dealloc{

	[_name release];
    [_image release];
    [_length release];
    [_startsWith release];
    [_barcodeType release];
    [_anotherLength release];
    
	[super dealloc];
}




@end