

#import <Foundation/Foundation.h>

@interface VendorItem : NSObject {

@private

	NSString* _name;
    NSString* _image;
    NSString* _length;
    NSString* _anotherLength;
    NSString* _startsWith;
    NSString* _barcodeType;
}

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* length;
@property (nonatomic, retain) NSString* image;
@property (nonatomic, retain) NSString* startsWith;
@property (nonatomic, retain) NSString* barcodeType;
@property (nonatomic, retain) NSString* anotherLength;
@end

