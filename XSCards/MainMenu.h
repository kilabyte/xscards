//
//  MainMenu.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-07.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdController.h"
#import <Foundation/Foundation.h>
#import "Card.h"
#import "BarcodeViewController.h"
#import "AddCard.h"
#import "HelpController.h"
#import "InfoController.h"
#import "SettingsController.h"
#import "PassScreenViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "SideSwipeTableViewController.h"

@class AppDelegate;
@interface MainMenu : SideSwipeTableViewController <UITableViewDataSource, UITableViewDelegate, ADBannerViewDelegate, MFMailComposeViewControllerDelegate>{
    IBOutlet UIButton *addButton;
    IBOutlet UIButton *editButton;
    IBOutlet UIButton *helpButton;
    IBOutlet UIButton *settingsButton;
    IBOutlet UIButton *infoButton;
    IBOutlet UIImageView *placeholderImage;
    IBOutlet UIImageView *swipeImage;
    
    IBOutlet UIButton *placeHolder;
    
    AdController *ads;
    
    NSMutableArray *_savedCards;
    AppDelegate *a;
    
    NSArray* buttonData;
    NSMutableArray* buttons;
    BOOL isSending;
    
    ADBannerView* iadView;
}

-(void)showLock;
-(IBAction)displayComposerSheetWithIndex:(int)index;
-(void)launchMailAppOnDevice;
-(void)editCardAtIndexPath:(NSIndexPath*)indexPath;
-(IBAction)showPicker:(NSIndexPath*)indexPath;
-(void)deleteCardAtIndex:(NSIndexPath*)indexPath;
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError withIndex:(int)index;
-(IBAction)reloadTable:(id)sender;
-(UIImage*)resizedImage1:(UIImage*)inImage  inRect:(CGRect)thumbRect;
@end
