//
//  HelpController.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdController.h"
#import "AppDelegate.h"

@interface HelpController : UIViewController {
    AdController *ads;
    IBOutlet UIScrollView *_scroll;
}

@end
