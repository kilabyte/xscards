//
//  Validator.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-10.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "Validator.h"

@implementation Validator
@synthesize waitForAlert;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performSelector:@selector(checkForWait) withObject:nil afterDelay:3.0];
    // Do any additional setup after loading the view from its nib.
}

-(void)checkForWait{
    NSTimer *t;
    int time = arc4random()%7;
    if (time <3) {
        time = 3;
    }
    if (time > 5) {
        time = 5;
    }
    
    if ([waitForAlert isEqualToString:@"wait"]) {
        [self performSelector:@selector(checkForWait) withObject:nil afterDelay:0.5];
    }
    else {
        t = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(done) userInfo:nil repeats:NO];
    }
}

-(void)done{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
return NO;
}

@end
