//
//  Card.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "Card.h"

@implementation Card
@synthesize image, name, number, mode;

- (id)init {
    self = [super init];
    if (self) {
        image = nil;
        name = @"";
        number = @"";
        mode = 0;
    }
    return self;
}


- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:image forKey:@"image"];
    [coder encodeInteger:mode forKey:@"mode"];
    [coder encodeObject:name forKey:@"name"];
    [coder encodeObject:number forKey:@"number"];
}

- (id)initWithCoder:(NSCoder *)coder;
{
    self = [[Card alloc] init];
    if (self != nil)
    {
        image = [[coder decodeObjectForKey:@"image"] retain];
        number = [[coder decodeObjectForKey:@"number"] retain];
        name = [[coder decodeObjectForKey:@"name"] retain];
        mode = [coder decodeIntForKey:@"mode"];
    }   
    return self;    
}
@end
