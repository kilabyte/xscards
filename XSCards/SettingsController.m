//
//  SettingsController.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "SettingsController.h"
#import "PassScreenViewController.h"

@implementation SettingsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)dealloc{
    [super dealloc];
    [ads release];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Settings";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelPassword) name:@"CancelPasswordSet" object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    a = appDelegate;
    ads = [[AdController alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-53, 320, 50)];
    [self.view insertSubview:ads.view atIndex:99];
    passwordSwitch = [[UISwitch alloc] init];
    soundSwitch = [[UISwitch alloc] init];
    cloudSwitch = [[UISwitch alloc] init];
    
    //LOAD SETTINGS
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL pass = [defaults boolForKey:@"PasswordState"];
    BOOL sound = [defaults boolForKey:@"ScannerSound"];
    BOOL cloud = [defaults boolForKey:@"CloudBackup"];
    [passwordSwitch setOn:pass];
    if (!sound) {
        [soundSwitch setOn:YES];
    }
    else {
        [soundSwitch setOn:sound];
    }
    
    [cloudSwitch setOn:cloud];
    setPassword = [defaults objectForKey:@"Password"];
    [defaults synchronize];
    
}
-(void)cancelPassword{
    [passwordSwitch setOn:NO];
    setPassword = NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    setPassword = [defaults objectForKey:@"Password"];
    [defaults setBool:passwordSwitch.isOn forKey:@"PasswordState"];
    [defaults synchronize];
}

-(void)viewDidDisappear:(BOOL)animated{
    //SAVE SETTINGS
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:passwordSwitch.isOn forKey:@"PasswordState"];
    [defaults setBool:soundSwitch.isOn forKey:@"ScannerSound"];
    [defaults setBool:cloudSwitch.isOn forKey:@"CloudBackup"];
    [defaults setObject:setPassword forKey:@"Password"];
    [defaults synchronize];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
return NO;
}


-(void)setPassword{
    if (passwordSwitch.isOn) {
        PassScreenViewController *p = [[PassScreenViewController alloc] initWithNibName:@"PassScreenViewController" bundle:nil];
        p.shouldSet = YES;
        [self presentModalViewController:p animated:YES];
        [p release];
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            //
            return 1;
            break;
        case 1:
            //
            return 1;
            break;
        case 2:
            //
            return 1;
            break;
        case 3:
            //
            return 3;
            break;
            
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    if (indexPath.section == 0) {
        //security
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Password";
            passwordSwitch.frame = CGRectMake(190, 9, 0, 0);
            [passwordSwitch addTarget:self action:@selector(setPassword) forControlEvents:UIControlEventValueChanged];
            [cell addSubview:passwordSwitch];
        }
    }
    if (indexPath.section == 1) {
        //scanner
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Sound";
            soundSwitch.frame = CGRectMake(190, 9, 0, 0);
            [soundSwitch addTarget:self action:nil forControlEvents:UIControlEventValueChanged];
            [cell addSubview:soundSwitch];
            
        }
    }
    if (indexPath.section == 2) {
        //iCloud
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Backup to iCloud";
            cloudSwitch.frame = CGRectMake(190, 9, 0, 0);
            [cloudSwitch addTarget:self action:nil forControlEvents:UIControlEventValueChanged];
            [cell addSubview:cloudSwitch];
        }
    }
    if (indexPath.section == 3) {
        //Contact
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Customer Support";
            cell.imageView.image = [UIImage imageNamed:@"email.png"];
        }
        if (indexPath.row == 1) {
            cell.textLabel.text = @"Share on Facebook";
            cell.imageView.image = [UIImage imageNamed:@"facebook.jpg"];
        }
        if (indexPath.row == 2) {
            cell.textLabel.text = @"Share on Twitter";
            cell.imageView.image = [UIImage imageNamed:@"twitter.jpg"];
        }
    }
    
    
    return cell;
}


#pragma mark - Table view delegate
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = nil;
    // Return a title or nil as appropriate for the section.
    switch (section) {
        case 0:
            title = @"Security";
            break;
        case 1:
            title = @"Scanner";
            break;
			
		case 2:
			title = @"iCloud";
			break;
        case 3:
			title = @"Contact";
			break;
        default:
            break;
    }
    return title;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 44.0)];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
//	headerLabel.textColor = [UIColor whiteColor];
//	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:16];
	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    
    switch (section) {
        case 0:
            headerLabel.text = @"Security";
            break;
        case 1:
            headerLabel.text = @"Scanner";
            break;
			
		case 2:
            headerLabel.text = @"iCloud";
			break;
        case 3:
            headerLabel.text = @"Contact";
			break;
        default:
            break;
    }
	[customView addSubview:headerLabel];
    
	return customView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section == 3 && indexPath.row == 0) {
        //email
        [self showPicker];
    }
    else if (indexPath.section == 3 && indexPath.row == 1) {
       // Facebook
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"FacebookLogin" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FacebookPost" object:nil];
    }
    else if (indexPath.section == 3 && indexPath.row == 2) {
        //Twitter
        if ([TWTweetComposeViewController canSendTweet]){
            TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
            [tweetSheet setInitialText:@"I use XS Cards for iOS to keep all my Gift and Loyalty cards in one spot. Check it out, its FREE! http://is.gd/swfEJn #XSCards #iOSApp"];
            [self presentModalViewController:tweetSheet animated:YES];
            [tweetSheet release];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"                                                          
                                                               delegate:self                                              
                                                      cancelButtonTitle:@"OK"                                                   
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    }
}

-(IBAction)showPicker{
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self displayComposerSheet];
	}
}	

#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(IBAction)displayComposerSheet {
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setSubject:@"XS Cards - Inquiry"];
    
    // Set up recipients
    NSArray *toRecipients = [NSArray arrayWithObject:@"support@interwerx.ca"]; 
    [picker setToRecipients:toRecipients];
    
    // Attach a doc to the email
    
    // Fill out the email body text
    //NSString *emailBody = @"Please see the attached document.";
    //[picker setMessageBody:emailBody isHTML:NO];
	
	[self presentModalViewController:picker animated:YES];
	[picker release];
}
// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Result: canceled");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Result: saved");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Result: sent");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Result: failed");
			break;
		default:
			NSLog(@"Result: not sent");
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Setup" message:@"You device is currently not setup to send emails. To setup an email account on your device go to : Settings -> Mail -> Add Account and continue with the directions."
												   delegate:self 
										  cancelButtonTitle:@"OK" 
										  otherButtonTitles:nil];
	[alert show];	
	[alert release];
}


@end
