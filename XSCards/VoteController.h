//
//  VoteController.h
//  XSCards
//
//  Created by Dave Sferra on 2012-03-14.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoteController : UIViewController{
    NSString *_cardName;
}

@property(nonatomic, retain) NSString *_cardName;

-(IBAction)yes:(id)sender;
-(IBAction)no:(id)sender;
-(void)show;
-(void)dismiss;
-(void)sendToServerWithResponse:(NSString*)answer andCardName:(NSString*)cardName;

@end
