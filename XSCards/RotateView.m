//
//  RotateView.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-10.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#
#import "RotateView.h"

@implementation RotateView

- (id)init {
    self = [super init];
    if (self) {
        self.layer.cornerRadius = 20;
    }
    return self;
}

-(void)awakeFromNib{
    self.layer.cornerRadius = 20;
}

@end
