//
//  BHTableViewCell.h
//  XSCards
//
//  Created by Dave Sferra on 2012-02-19.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BHTableViewCell : UITableViewCell{
    BOOL supressDeleteButton;
}

@property (nonatomic) BOOL supressDeleteButton;

@end
