//
//  RotateView.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-10.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface RotateView : UIView

@end
