//
//  SettingsController.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import "AppDelegate.h"
#import "AdController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SettingsController : UIViewController <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>{
    IBOutlet UITableView *settingsTable;
    UISwitch *passwordSwitch;
    UISwitch *soundSwitch;
    UISwitch *cloudSwitch;
    AppDelegate *a;
    NSString *setPassword;
    AdController *ads;
}


-(IBAction)displayComposerSheet;
-(IBAction)showPicker;
-(void)launchMailAppOnDevice;
@end
