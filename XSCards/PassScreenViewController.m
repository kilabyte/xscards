//
//  PassScreenViewController.m
//  passProtect
//

#import "PassScreenViewController.h"

@implementation PassScreenViewController
@synthesize shouldSet;

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	if(textField == v1 || textField == v2 || textField == v3 || textField == v4)
    {
		[nv1 becomeFirstResponder];
        v1.backgroundColor = [UIColor whiteColor];
        v2.backgroundColor = [UIColor whiteColor];
        v3.backgroundColor = [UIColor whiteColor];
        v4.backgroundColor = [UIColor whiteColor];
		return NO;
	}
	return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidLoad];
    if (!shouldSet) {
        [navBar setHidden:YES];
    }
    else {
        [navBar setHidden:NO];
    }
	nv1.text = @" ";
	nv1.text = @" ";
	nv1.text = @" ";
	nv1.text = @" ";
    [nv1 becomeFirstResponder];
    a = appDelegate;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	//Everything below will emulate a passcode screen behaviour.
	if(range.length != 1)
    { //The user is inputting characters.
        v1.backgroundColor = [UIColor whiteColor];
        v2.backgroundColor = [UIColor whiteColor];
        v3.backgroundColor = [UIColor whiteColor];
        v4.backgroundColor = [UIColor whiteColor];
		if(textField == nv1)
        {
			[nv2 becomeFirstResponder];
			v1.text = string;
		}else if(textField == nv2)
        {
			[nv3 becomeFirstResponder];
			v2.text = string;
		}else if(textField == nv3)
        {
			[nv4 becomeFirstResponder];
			v3.text = string;
		}else if(textField == nv4)
        {
			[nv4 resignFirstResponder];
			v4.text = string;
			textField.text = string; //Needed because the last textfield won't have a value because this executes before the other textField.text statement below.
			
			NSMutableString *pass = [NSMutableString stringWithString:nv1.text];
			[pass appendString:nv2.text];
			[pass appendString:nv3.text];
			[pass appendString:nv4.text];
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *p = [defaults objectForKey:@"Password"];
            if (!shouldSet) {
                if(![pass isEqualToString:p]) //Passwords don't match. Hypothetical password here is 5555.
                {
                    UIAlertView *passDontMatch = [[UIAlertView alloc] initWithTitle:@"Incorrect Password" message:@"Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    //[passDontMatch show];
                    nv1.text = @" ";
                    nv2.text = @" ";
                    nv3.text = @" ";
                    nv4.text = @" ";
                    v1.text = nil;
                    v2.text = nil;
                    v3.text = nil;
                    v4.text = nil;
                    v1.backgroundColor = [UIColor redColor];
                    v2.backgroundColor = [UIColor redColor];
                    v3.backgroundColor = [UIColor redColor];
                    v4.backgroundColor = [UIColor redColor];
                    AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
                    [passDontMatch release];
                    [nv1 becomeFirstResponder];
                }else
                {
                    v1.backgroundColor = [UIColor greenColor];
                    v2.backgroundColor = [UIColor greenColor];
                    v3.backgroundColor = [UIColor greenColor];
                    v4.backgroundColor = [UIColor greenColor];
                    [self performSelector:@selector(unlock) withObject:nil afterDelay:.6];
                }
            }
            else {
                [defaults setObject:pass forKey:@"Password"];
                [self dismissModalViewControllerAnimated:YES];
            }

			
		}
	}else
    { //The user is deleting characters.
		string = @" ";
		if(textField == nv4)
        {
			nv4.text = @" ";
			nv3.text = @" ";
			v4.text = nil;
			v3.text = nil;
			[nv3 becomeFirstResponder];
		}else if(textField == nv3)
        {
			nv3.text = @" ";
			nv2.text = @" ";
			v3.text = nil;
			v2.text = nil;
			[nv2 becomeFirstResponder];
		}else if(textField == nv2)
        {
			nv2.text = @" ";
			nv1.text = @" ";
			v2.text = nil;
			v1.text = nil;
			[nv1 becomeFirstResponder];
		}else if(textField == nv1)
        {
			nv1.text = @" ";
			v1.text = nil;
		}
	}
	textField.text = string;
	return NO;
}
-(IBAction)cancel:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CancelPasswordSet" object:nil];
    [self dismissModalViewControllerAnimated:YES];
}

-(void)unlock{
    AudioServicesPlaySystemSound (1101);
    [self dismissModalViewControllerAnimated:YES];
}

@end
