//
//  AppDelegate.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-07.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "AppDelegate.h"

#import "MainMenu.h"
#import "Mobclix.h"
#import "Appirater.h"
#import "MKStoreManager.h"


@implementation AppDelegate

@synthesize window = _window;
@synthesize mainMenu = _mainMenu;
@synthesize navigationController;
@synthesize _cardInputed, _mode, _cardScanned, _type, password, shouldShowLock, _image, sendNewCard;
@synthesize  _storedCards, facebook, adRect;

- (void)dealloc
{
    [_window release];
    [_mainMenu release];
    [_cardInputed release];
    [_storedCards release];
    [_cardScanned release];
    [_type release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    //[MKStoreManager sharedManager];
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)]; 
    
	
	// RESET THE BADGE COUNT
    application.applicationIconBadgeNumber = 0; 
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postToFacebook) name:@"FacebookPost" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginToFacebook) name:@"FacebookLogin" object:nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSUbiquitousKeyValueStore *cloud = [NSUbiquitousKeyValueStore defaultStore];
    BOOL sound = [defaults boolForKey:@"ScannerSound"];
    if (!sound) {
        sound = YES;
        [defaults setBool:sound forKey:@"ScannerSound"];
    }
    saved = [defaults boolForKey:@"Save"];
    _cardInputed = [defaults objectForKey:@"CardNumber"];
    _mode = [defaults integerForKey:@"Mode"];
    //BOOL shouldUseBackup = [defaults boolForKey:@"CloudBackup"];
    NSData *dataRepresentingSavedArray = [cloud objectForKey:@"Cards"];
    
    if (!dataRepresentingSavedArray) {
        dataRepresentingSavedArray = [defaults objectForKey:@"SavedCards"];
    }
    
    if (dataRepresentingSavedArray != nil){
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray != nil){
            _storedCards = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        }
        else{
            _storedCards = [[NSMutableArray alloc] init];
        }
    }
    else {
        _storedCards = [[NSMutableArray alloc] init];
    }
    [defaults synchronize];
    self.mainMenu = [[[MainMenu alloc] initWithNibName:@"MainMenu" bundle:nil] autorelease];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.mainMenu];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.window.rootViewController = self.navigationController;
    [Mobclix startWithApplicationId:@"E89AB764-F7C4-45A3-83A5-D7CE3FE0D9AA"];
    [Appirater appLaunched];
    [self.window makeKeyAndVisible];
    _loadingC = [[LoadingViewController alloc] initWithNibName:@"LoadingViewController" bundle:[NSBundle mainBundle]];
    _loadingC.useFixedTime = YES;
    [self.window addSubview:_loadingC.view];
    return YES;
}

static BOOL _isDataSourceAvailable;

+ (BOOL)isDataSourceAvailable
{
    static BOOL checkNetwork = YES;
    if (checkNetwork) { // Since checking the reachability of a host can be expensive, cache the result and perform the reachability check once.
        checkNetwork = NO;
        
        BOOL success;    
		
		const char *host_name = "bluehawksolutions.com";
		
        SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host_name);
        SCNetworkReachabilityFlags flags;
        success = SCNetworkReachabilityGetFlags(reachability, &flags);
        _isDataSourceAvailable = success && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
        CFRelease(reachability);
    }
    return _isDataSourceAvailable;
}

# pragma START push

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken { 
    [self sendDeviceTokenToRemote:devToken];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err { 
    NSLog(@"Failed to register, error: %@", err); 
} 

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
	NSLog( @"didReceiveRemoteNotification:%@", userInfo );
    
}

-(void)sendDeviceTokenToRemote:(NSData *)deviceToken
{
	//NSLog( @"sendDeviceTokenToRemote:%@", deviceToken );
	if ([AppDelegate isDataSourceAvailable]) {
        NSString *inDeviceTokenStr = [deviceToken description];
        NSString *deviceName = [[UIDevice currentDevice] name];
        NSString *tokenString = [inDeviceTokenStr stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"< >"]];
        tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:tokenString forKey:@"UUID"];
        [defaults synchronize];
        // send it to the remote server
        
        NSString *hostString = @"http://interwerx.ca/iOS/Push/push_xscards.php";
        NSString *nameString = deviceName;
        nameString = [nameString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *argsString = @"%@?token=%@&cmd=reg&name=%@";
        NSString *getURLString = [NSString stringWithFormat:argsString,hostString,tokenString,nameString];
        NSString *registerResult = [NSString stringWithContentsOfURL:[NSURL URLWithString:getURLString] encoding:NSStringEncodingConversionAllowLossy error:nil];
        
        NSLog( @"registerResult:%@", registerResult );
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:hostString]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[[[NSData alloc] initWithBytes:[registerResult UTF8String] length:[registerResult length]] autorelease]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [[NSURLConnection connectionWithRequest:request delegate:self] start];
    }

    
	
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    [Appirater appEnteredForeground:NO];
    //save the users cards to file
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_cardInputed forKey:@"CardNumber"];
    [defaults setInteger:_mode forKey:@"Mode"];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:_storedCards] forKey:@"SavedCards"];
    [defaults synchronize];
    
    //save user prefs to icloud
    NSUbiquitousKeyValueStore *cloud = [NSUbiquitousKeyValueStore defaultStore];
    BOOL shouldBackup = [defaults boolForKey:@"CloudBackup"];
    if (cloud && shouldBackup) {
        [cloud setObject:[NSKeyedArchiver archivedDataWithRootObject:_storedCards] forKey:@"Cards"];
        NSLog(@"Uploaded cards to iCloud!");
    }
    [cloud synchronize];
    
}

-(NSData *)getImageFromUrlWithName:(NSString*)name{
    //get image from url to load
    NSString *string = [NSString stringWithFormat:@"http://www.interwerx.ca/iOS/VendorImages/%@",name];
    NSURL *imageURL = [NSURL URLWithString:string];
	NSData *imageData;
	imageData = [NSData dataWithContentsOfURL:imageURL];
    return imageData;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
#ifdef DEBUG
    NSLog(@"Open URL:\t%@\n"
          "From source:\t%@\n"
          "With annotation:%@",
          url, sourceApplication, annotation);
#endif
    NSString *urlMod = [url absoluteString];
    NSArray *parts = [urlMod componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    filename = [filename stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Inbox/%@",filename]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:savePath];
    NSString *filepath = [url scheme];
    
    if ([filepath rangeOfString:@"xscards"].location != NSNotFound){
        NSLog(@"URL");
    }
    else if (fileExists){
        NSLog(@"File");
        Card *item = [[Card alloc] init];
        item = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithContentsOfFile:savePath]];
        if (item != nil) {
            [_storedCards addObject:item];
            sendNewCard = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadCards" object:nil];
        }
        else {
            UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Card is corrupted..." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [al show];
            [al release];
        }

        [item release];
        
        if (fileExists) {
            [[NSFileManager defaultManager] removeItemAtPath:savePath error:nil];
        }
    }
    
    if ([sourceApplication rangeOfString:@"facebook"].location != NSNotFound) {
        NSLog(@"Facebook");
        [self performSelector:@selector(post) withObject:nil afterDelay:1.0];
        return [facebook handleOpenURL:url];
    }
    return YES;
}
-(void)loginToFacebook{
    facebook = [[Facebook alloc] initWithAppId:@"136165926505790" andDelegate:self];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] && [defaults objectForKey:@"FBExpirationDateKey"]) {
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    if (![facebook isSessionValid]) {
        NSArray *permissions = [[NSArray alloc] initWithObjects: 
                                @"read_stream",
                                @"publish_stream",
                                nil];
        [facebook authorize:permissions];
        [permissions release];
    } 
}
- (void)fbDidLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    
}
-(void)post{
    NSMutableDictionary *params = 
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     @"I'm using XSCards for iOS app", @"name",
     @"XS Cards for iOS", @"caption",
     @"XS Cards is a simple solution to manage all your membership, loyalty and gift cards. Keep all your \"cards\" secured in one spot for you to use at anytime. No sign-up is necessary no email account to set-up. Its that simple! ", @"description",
     @"https://m.facebook.com/apps/xscards/", @"link",
     @"http://interwerx.ca/XSCardsReasources/iconIOS512.tif", @"picture",
     nil];  
    [facebook dialog:@"feed"
           andParams:params
         andDelegate:self];
}
-(void)postToFacebook{
    if (![facebook isSessionValid]) {
        [self performSelector:@selector(loginToFacebook)];
    }
    else {
        NSMutableDictionary *params = 
        [NSMutableDictionary dictionaryWithObjectsAndKeys:
         @"I'm using XSCards for iOS app", @"name",
         @"XS Cards for iOS", @"caption",
         @"XS Cards is a simple solution to manage all your membership, loyalty and gift cards. Keep all your \"cards\" secured in one spot for you to use at anytime. No sign-up is necessary no email account to set-up. Its that simple! ", @"description",
         @"https://m.facebook.com/apps/xscards/", @"link",
         @"http://interwerx.ca/XSCardsReasources/iconIOS512.tif", @"picture",
         nil];  
        [facebook dialog:@"feed"
               andParams:params
             andDelegate:self];
    }

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
    // RESET THE BADGE COUNT
    application.applicationIconBadgeNumber = 0; 
    [Appirater appEnteredForeground:YES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL check = [defaults boolForKey:@"PasswordState"];
    if (check) {//shouldShowLock BOOL
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LockOn" object:nil];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    [facebook logout];
}

@end
