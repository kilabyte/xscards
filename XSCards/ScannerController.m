//
//  ScannerController.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "ScannerController.h"

@implementation ScannerController
@synthesize readerView, data;


- (void) cleanup
{
    [cameraSim release];
    cameraSim = nil;
    readerView.readerDelegate = nil;
    [readerView release];
    readerView = nil;
}

- (void) dealloc
{
    [self cleanup];
    [super dealloc];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    a = appDelegate;
    // the delegate receives decode results
    readerView.readerDelegate = self;
    readerView.tracksSymbols = YES;
    readerView.trackingColor = [UIColor colorWithRed:0.49803922 green:.68235294 blue:.25490196 alpha:1.0];
    readerView.backgroundColor = [UIColor blackColor];
    [readerView.scanner setSymbology:ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 1];
    [readerView.scanner setSymbology:ZBAR_UPCE config: ZBAR_CFG_ENABLE to: 1];
    [readerView.scanner setSymbology:ZBAR_I25 config: ZBAR_CFG_ENABLE to: 1];
    [readerView.scanner setSymbology:ZBAR_ISBN10 config: ZBAR_CFG_ENABLE to: 1];
    [readerView.scanner setSymbology:ZBAR_ISBN13 config: ZBAR_CFG_ENABLE to: 1];
    [readerView.scanner setSymbology:ZBAR_EAN2 config: ZBAR_CFG_ENABLE to: 1];
    [readerView.scanner setSymbology:ZBAR_EAN5 config: ZBAR_CFG_ENABLE to: 1];
    [readerView.scanner setSymbology:ZBAR_COMPOSITE config: ZBAR_CFG_ENABLE to: 1];
    
    // you can use this to support the simulator
    if(TARGET_IPHONE_SIMULATOR) {
        cameraSim = [[ZBarCameraSimulator alloc] initWithViewController: self];
        cameraSim.readerView = readerView;
    }
}

- (void) viewDidUnload{
    [self cleanup];
    [super viewDidUnload];
}

-(IBAction)done:(id)sender{
    a._cardScanned = data;
    [self dismissModalViewControllerAnimated:YES];
}
-(IBAction)cancel:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}
-(IBAction)noBarCode:(id)sender{
    
}

- (BOOL) shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation) orient
{
    // auto-rotation is supported
    return(YES);
}

- (void) willRotateToInterfaceOrientation: (UIInterfaceOrientation) orient duration: (NSTimeInterval) duration{
    // compensate for view rotation so camera preview is not rotated
    [readerView willRotateToInterfaceOrientation: orient duration: duration];
}

- (void) viewDidAppear: (BOOL) animated
{
    // run the reader when the view is visible
    [readerView start];
}

- (void) viewWillDisappear: (BOOL) animated
{
    [readerView stop];
}

-(IBAction)shoot{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL shouldSound = [defaults boolForKey:@"ScannerSound"];
    if (shouldSound) {
        AudioServicesPlaySystemSound (1057);
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }

}

- (void) readerView: (ZBarReaderView*) view didReadSymbols: (ZBarSymbolSet*) syms fromImage: (UIImage*) img{
    // do something useful with results
    for(ZBarSymbol *sym in syms) {
        a._type = sym.typeName;
        [a._type retain];
        if ([a._type isEqualToString:@"UPC-A"]) {
            NSString *s = sym.data;
            s = [s substringToIndex:[s length] - 1];
            data = s;
        }
        else {
            data = sym.data;
        }
        [data retain];
        [self performSelector:@selector(done:) withObject:nil afterDelay:.8];
        [self shoot];
        break;
    }
}


@end
