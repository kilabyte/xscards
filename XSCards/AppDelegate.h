//
//  AppDelegate.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-07.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingViewController.h"
#import "FBConnect.h"
#import <SystemConfiguration/SystemConfiguration.h>

@class MainMenu;
@class BarcodeViewController;
#define appDelegate	(AppDelegate *)[[UIApplication sharedApplication] delegate]

@interface AppDelegate : UIResponder <UIApplicationDelegate, FBSessionDelegate, FBDialogDelegate, FBRequestDelegate>{
    IBOutlet UINavigationController *navigationController;
    NSString *_cardInputed;
    int _mode;
    NSMutableArray *_storedCards;
    BOOL saved;
    NSString *_cardScanned;
    NSString *_type;
    NSString *password;
    BOOL showShowLock;
    LoadingViewController *_loadingC;
    UIImage *_image;
    BOOL sendNewCard;
    Facebook *facebook;
    CGRect adRect;
}
@property (nonatomic, retain) Facebook* facebook;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSString *_cardInputed;
@property (nonatomic, retain) NSMutableArray *_storedCards;
@property (assign) int _mode;
@property (assign) BOOL shouldShowLock;
@property (assign) BOOL sendNewCard;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *_cardScanned;
@property (nonatomic, retain) NSString *_type;
@property (nonatomic, retain) UIImage *_image;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (strong, nonatomic) MainMenu *mainMenu;
@property(assign) CGRect adRect;
+ (BOOL)isDataSourceAvailable;
-(NSData *)getImageFromUrlWithName:(NSString*)name;

@end
