//
//  BarcodeViewController.h
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NKDBarcodeFramework.h"
#import "AppDelegate.h"
#import "RotateView.h"

@class AppDelegate;

@interface BarcodeViewController : UIViewController {
    AppDelegate *a;
    IBOutlet UIImageView *_barCodeView;
    IBOutlet UIImageView *_vendorLogo;
    IBOutlet UILabel *_caption;
    IBOutlet UIView *rView;
    NSString *nameTitle;
    IBOutlet UINavigationBar *navBar;
    int modes;
}

@property(nonatomic, retain)IBOutlet UILabel *_caption;
@property(nonatomic, retain) NSString *nameTitle;
-(void)checkForRotate;
-(void)generateBarcodeWithMode:(int)mode;
@end
