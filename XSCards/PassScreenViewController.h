//
//  PassScreenViewController.h
//  passProtect
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>

@class AppDelegate;
@interface PassScreenViewController : UIViewController <UITextFieldDelegate>{
    IBOutlet UITextField *v1, *v2, *v3, *v4; //Visible textfields.
    IBOutlet UITextField *nv1, *nv2, *nv3, *nv4; //Invisible textfields.
    BOOL shouldSet;
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIBarButtonItem *cancelButton;
    
    AppDelegate *a;
}
@property(assign)BOOL shouldSet;

@end
