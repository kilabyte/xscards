//
//  MKStoreKitConfigs.h
//  MKStoreKit (Version 4.0)
//



// To avoid making mistakes map plist entries to macros on this page.
// when you include MKStoreManager in your clss, these macros get defined there

#define kConsumableBaseFeatureId @"com.mycompany.myapp."
#define kFeatureAId @"com.bluehawksolutions.XSCards.AdRemoval"
#define kConsumableFeatureBId @"com.mycompany.myapp.005"

#define SERVER_PRODUCT_MODEL 0
#define OWN_SERVER nil
#define REVIEW_ALLOWED 0

#define kSharedSecret @"<FILL IN YOUR SHARED SECRET HERE>"
