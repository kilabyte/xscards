//
//  LoadingViewController.h
//  CoastLinePilots
//
//  Created by Dave Sferra on 11-07-17.
//  Copyright 2011 Blue Hawk Solutions inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoadingViewController : UIViewController {
    IBOutlet UIImageView *_mainImage;
    IBOutlet UIActivityIndicatorView *_actView;
    BOOL useFixedTime;
    int fixedTime;
    
}
@property(assign)BOOL useFixedTime;
@property(assign)int fixedTime;
-(void)finished;
@end
