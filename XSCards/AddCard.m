//
//  AddCard.m
//  XSCards
//
//  Created by Dave Sferra on 12-01-09.
//  Copyright (c) 2012 Blue Hawk Solutions inc. All rights reserved.
//

#import "AddCard.h"
#import "MKStoreManager.h"


@implementation AddCard

@synthesize isEditing, editIndex, editCard;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        a = appDelegate;
        selectedMode = 0;
        barcodeTypes = [[NSMutableArray alloc] init];
        [barcodeTypes addObject:@"EAN-8"];
        [barcodeTypes addObject:@"EAN-13"];
        [barcodeTypes addObject:@"CODABAR"];
        [barcodeTypes addObject:@"CODE-39"];
        [barcodeTypes addObject:@"CODE-128"];
        [barcodeTypes addObject:@"UPC-A"];
        [barcodeTypes addObject:@"UPC-E"];
        [barcodeTypes addObject:@"UPC-EAN"];
        [barcodeTypes addObject:@"I2/5"];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc{
    [super dealloc];
    v = nil;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [name becomeFirstResponder];
    isChecking = NO;
    v = nil;
    didUseOnline = NO;
    if (isEditing) {
        number.text = editCard.number;
        name.text = editCard.name;
        selectedMode = editCard.mode;
        navBar.topItem.title = @"Edit Card";
        [startHere setHidden:YES];
    }
    else {
        navBar.topItem.title = @"Add Card";
        [startHere setHidden:NO];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    showScanner = NO;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (v && isChecking) {
        [self dismissModalViewControllerAnimated:YES];
        isChecking = NO;
        isEditing = NO;
        v = nil;
        wait = NO;
    }
    else if (showScanner == NO && !isEditing){
        [self openScanner:nil];
        showScanner = YES;
    }
    
    
}

-(void)validatorWithNoCode:(BOOL)useCode{
    if ([AppDelegate isDataSourceAvailable]) {
        //get xml from server for vendor list
        xmlVendor = [[XMLVendor alloc] init];
        VendorItem *item = [[VendorItem alloc] init];
        vendorList = [[NSMutableArray alloc] init];
        if (useCode == YES) {
            [xmlVendor parseXMLFileAtURL:[NSURL URLWithString:@"http://www.interwerx.ca/iOS/vendorList.xml"]];
        }
        else {
            [xmlVendor parseXMLFileAtURL:[NSURL URLWithString:@"http://www.interwerx.ca/iOS/vendorListNoCode.xml"]];
            barcodeType = @"CODE-128";  
        }
        vendorList = xmlVendor.results;
        
        if (!isEditing && number.text != nil) {
            //run validator
            for (int i = 0; i<[vendorList count]; i++) {
                item = [vendorList objectAtIndex:i];
                if ([number.text rangeOfString:item.startsWith].location != NSNotFound) {
#ifdef DEBUG
                    NSLog(@"Matched Starts With");
#endif
                    if (number.text.length == [item.length intValue] || number.text.length == [item.anotherLength intValue]) {
#ifdef DEBUG
                        NSLog(@"Matched length 1 or 2");
                        NSLog(@"%@",item.barcodeType);
#endif
                        if ([barcodeType isEqualToString:item.barcodeType]) {
                            //WE FOUND A MATCH SO ASK THE USER
                            foundIndex = i;
                            UIAlertView *al = [[UIAlertView alloc] initWithTitle:item.name message:[NSString stringWithFormat:@"Is this a %@ card?",item.name] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                            if (useCode == NO) {
                                wait = YES;
                            }
                            [al show];
                            [al release];
                            break;
                        }
                    }
                }
            }
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    VendorItem *item = [[VendorItem alloc] init];
    item = [vendorList objectAtIndex:foundIndex];
    [item retain];
    BHImage *dImage = [[BHImage alloc] initWithData:[a getImageFromUrlWithName:item.image]];
    
    if ([alertView.title isEqualToString:item.name]) {
        if (v && wait) {
            wait = NO;
            v.waitForAlert = @"no";
        }
        switch (buttonIndex) {
            case 0:
                //cancel   
                image = nil;
                
                break;
            case 1:
                //yes
                didUseOnline = YES;
                name.text = item.name;
                image = dImage;
                [self save:nil];
            default:
                break;
        }
    }
    [item release];
    [dImage release];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
        if (a._cardScanned.length > 0) {
            number.text = a._cardScanned;
            a._cardScanned = nil;
        }
        [self performSelector:@selector(checkForSave:)];
        if (a._type.length > 0) {
            barcodeType = a._type;
            a._type = nil;
            [barcodeType retain];
            [self findBarcodeType];
            [self validatorWithNoCode:YES];
        }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (isChecking == NO) {
        isChecking = YES;
    }

}

-(IBAction)openScanner:(id)sender{
    [ZBarReaderView class];
    ScannerController *e = [[ScannerController alloc] initWithNibName:@"ScannerController" bundle:nil];
    [self presentModalViewController:e animated:YES];
    [e release];
}

-(IBAction)save:(id)sender{
    v = [[Validator alloc] initWithNibName:@"Validator" bundle:nil];
    [self findBarcodeType];
    if (didUseOnline == NO) {
       [self validatorWithNoCode:NO];
    }
    
    if(wait){
        v.waitForAlert = @"wait";
    }
    else {
        v.waitForAlert = @"no";
        
        Card *g = [[Card alloc] init];
        [g setName:name.text];
        [g setNumber:number.text];
        [g setMode:selectedMode];
        if (image) {
            [g setImage:image];
        }
        else {
            [g setImage:nil];
        }
        
        if (name.text.length != 0) {
            if (!isEditing) {
                
                [a._storedCards addObject:g];
            }
            else {
                //editing
                [a._storedCards replaceObjectAtIndex:editIndex withObject:g];
            }
        }
        
        [g release];
    }
    [self performSelector:@selector(sendToPoll)];
    [self presentModalViewController:v animated:YES];
    
}

-(void)sendToPoll{
    if ([AppDelegate isDataSourceAvailable]) {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *inDeviceTokenStr = nil;
    if (![defaults objectForKey:@"UUID"]) {
        inDeviceTokenStr = [[NSProcessInfo processInfo] globallyUniqueString];
        [defaults setObject:inDeviceTokenStr forKey:@"UUID"];
    }
    else {
        inDeviceTokenStr = [defaults objectForKey:@"UUID"];
    }
    NSString *vendorName = name.text;
    NSString *deviceName = [[UIDevice currentDevice] name];
    NSString *tokenString = [inDeviceTokenStr stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"< >"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *hostString = @"http://interwerx.ca/iOS/reportToPoll.php";
    NSString *nameString = deviceName;
    nameString = [nameString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    vendorName = [vendorName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *argsString = @"%@?token=%@&card=%@&name=%@&type=%@&number=%@";
    NSString *getURLString = [NSString stringWithFormat:argsString, hostString, tokenString, vendorName, nameString, [barcodeTypes objectAtIndex:selectedMode], number.text];
    NSString *registerResult = [NSString stringWithContentsOfURL:[NSURL URLWithString:getURLString] encoding:NSStringEncodingConversionAllowLossy error:nil];
    //NSLog( @"registerResult:%@", registerResult ); 
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:hostString]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[[NSData alloc] initWithBytes:[registerResult UTF8String] length:[registerResult length]] autorelease]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [[NSURLConnection connectionWithRequest:request delegate:self] start];
    }
}

-(IBAction)checkForSave:(id)sender{
    if (![number.text isEqualToString:@""] && ![name.text isEqualToString:@""]) {
        doneButton.enabled = YES;
    }
    else {
        doneButton.enabled = NO;
    }
}
-(IBAction)cancel:(id)sender{
    isEditing = NO;
    [self dismissModalViewControllerAnimated:YES];
}

-(void)findBarcodeType{
    BOOL shouldDefault = YES;
    for (int i = 0; i < [barcodeTypes count]; i++) {
        if ([barcodeType isEqualToString:[barcodeTypes objectAtIndex:i]]) {
            selectedMode = i;
            shouldDefault = NO;
#ifdef DEBUG
            NSLog(@"Matched Type - %@",[barcodeTypes objectAtIndex:i]);
#endif
            break;
        }
    }
    if (shouldDefault) {
        selectedMode = 4;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
return NO;
}

@end
